#include <iostream>
#include <string>

class Animal {
private:
    int test; // used only for demo of initialization

protected:
    std::string name;

public:

    // __init__
    Animal() : name("No Name"), test(10) {
        // name = "No Name";
    }

    Animal(std::string name) : name(name), test(5)  {
        // this->name = name;
    }

    void greet() {
        std::cout << "Hi from " << this->name << std::endl;
    }
};

class Cat : public Animal {
private:
    int claw_length;
public:
    Cat(std::string name, int claw_length) : Animal(name), claw_length(claw_length) {
    }

    void greet() {
        std::cout << "Hi from cat " << name << " with claws of len " << claw_length << std::endl;
    }
};

class Dog : public Animal {
private:
    int tail_size;
public:
    Dog(std::string name, int tail_size) : Animal(name), tail_size(tail_size) {
    }

    void greet() {
        std::cout << "Hi from dog " << name << " with " << (tail_size > 5 ? "big" : "small") << " tail!" << std::endl;
    }
};

int main() {

    Animal a;
    a.greet();

    Cat cat("Murchik", 3);
    cat.greet();

    Dog dog("SmallBall", 10);
    dog.greet();

    return 0;
}
